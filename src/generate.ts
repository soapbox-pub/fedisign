interface GenerateKeyPairOpts {
  /**
   * Strength of the key, eg `2048`, `4096`, etc. The default is `4096` which is very secure but is SLOW. Using a smaller number like `1024` will be fast, but less secure.
   *
   * See here for advice on which bit size to use: https://stackoverflow.com/a/589850
   */
  bits?: number;
}

/** Generate an RSA keypair for use on the Fediverse. */
function generateKeyPair(opts?: GenerateKeyPairOpts): Promise<CryptoKeyPair> {
  return crypto.subtle.generateKey(
    {
      name: 'RSASSA-PKCS1-v1_5',
      modulusLength: opts?.bits ?? 2048,
      publicExponent: new Uint8Array([1, 0, 1]),
      hash: 'SHA-256',
    },
    true,
    ['sign', 'verify'],
  );
}

export { generateKeyPair };
