import { arrayBufferToBase64, str2ab } from './utils.ts';

/** Convert an RSA public key into PEM format. */
async function publicKeyToPem(publicKey: CryptoKey): Promise<string> {
  const buffer = await crypto.subtle.exportKey('spki', publicKey);
  const base64 = arrayBufferToBase64(buffer);
  return `-----BEGIN PUBLIC KEY-----\n${base64}\n-----END PUBLIC KEY-----`;
}

/** Convert an RSA private key into PEM format. */
async function privateKeyToPem(privateKey: CryptoKey): Promise<string> {
  const buffer = await crypto.subtle.exportKey('pkcs8', privateKey);
  const base64 = arrayBufferToBase64(buffer);
  return `-----BEGIN PRIVATE KEY-----\n${base64}\n-----END PRIVATE KEY-----`;
}

/** Convert a PEM public key into a CryptoKey. */
function pemToPublicKey(pem: string): Promise<CryptoKey> {
  pem = pem.trim();
  // fetch the part of the PEM string between header and footer
  const pemHeader = '-----BEGIN PUBLIC KEY-----';
  const pemFooter = '-----END PUBLIC KEY-----';
  const pemContents = pem.substring(pemHeader.length, pem.length - pemFooter.length);

  // base64 decode the string to get the binary data
  const binaryDerString = atob(pemContents);
  // convert from a binary string to an ArrayBuffer
  const binaryDer = str2ab(binaryDerString);

  return crypto.subtle.importKey(
    'spki',
    binaryDer,
    {
      name: 'RSASSA-PKCS1-v1_5',
      hash: 'SHA-256',
    },
    true,
    ['verify'],
  );
}

/** Convert a PEM private key into a CryptoKey. */
function pemToPrivateKey(pem: string): Promise<CryptoKey> {
  pem = pem.trim();
  // fetch the part of the PEM string between header and footer
  const pemHeader = '-----BEGIN PRIVATE KEY-----';
  const pemFooter = '-----END PRIVATE KEY-----';
  const pemContents = pem.substring(pemHeader.length, pem.length - pemFooter.length);

  // base64 decode the string to get the binary data
  const binaryDerString = atob(pemContents);
  // convert from a binary string to an ArrayBuffer
  const binaryDer = str2ab(binaryDerString);

  return crypto.subtle.importKey(
    'pkcs8',
    binaryDer,
    {
      name: 'RSASSA-PKCS1-v1_5',
      hash: 'SHA-256',
    },
    true,
    ['sign'],
  );
}

export { pemToPrivateKey, pemToPublicKey, privateKeyToPem, publicKeyToPem };
