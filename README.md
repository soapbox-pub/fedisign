# Fedisign

A batteries-included library for dealing with actor keys on the Fediverse. Generate keys, import/export into PEM format, sign requests and verify requests with HTTP signatures.

It is heavily based on Cloudflare's [Wildebeest](https://github.com/cloudflare/wildebeest), which includes code from [node-http-signature](https://github.com/TritonDataCenter/node-http-signature).

## Usage

```ts
import fedisign from 'https://gitlab.com/soapbox-pub/fedisign/-/raw/v0.2.1/mod.ts';
```

### Generate keys

```ts
generateKeyPair(opts?: GenerateKeyPairOpts): Promise<CryptoKeyPair>
```

Generates a Fediverse-compatible RSA keypair. You can store this in your database with the PEM functions below. Example:

```ts
const keys = await generateKeyPair({ bits: 2048 });
```

### Sign a request

```ts
signRequest(request: Request, key: CryptoKey, keyId: string): Promise<void>
```

Signs the `Request` object by adding a `Signature` and `Digest` header.
For example:

```ts
const request = new Request('https://soapbox.pub/inbox', {
  method: 'POST',
  body: '{"hello":"world"}',
});

await signRequest(
  request,
  keys.privateKey,
  'https://soapbox.pub/users/alex#main-key',
);

request; // The request now has a `Signature` and `Digest` header.
```

### Verify a request

```ts
verifyRequest(request: Request, getPubKey: PubKeyGetter): Promise<boolean>
```

Ensure the request has a valid signature. The second argument should be a function to get the actor's key from the parsed signature.

```ts
const valid = verifyRequest(request, async (parsedSignature) => {
  const response = await fetch(parsedSignature.keyId, {
    headers: { Accept: 'application/activity+json' },
  });

  if (response.ok) {
    const actor = await response.json();
    return pemToPublicKey(actor.publicKey.publicKeyPem);
  } else {
    return null;
  }
});

valid; // will be true or false, or might throw.
```

## Converting to and from PEM

For converting to and from PEM, use the following functions:

```ts
publicKeyToPem(publicKey: CryptoKey): Promise<string>
privateKeyToPem(privateKey: CryptoKey): Promise<string>
pemToPublicKey(pem: string): Promise<CryptoKey>
pemToPrivateKey(pem: string): Promise<CryptoKey>
```

## License

© Cloudflare, Inc.\
© Joyent, Inc.\
© Alex Gleason

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
