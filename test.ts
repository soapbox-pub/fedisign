import { assert, assertRejects } from 'https://deno.land/std@0.218.2/assert/mod.ts';

import {
  generateKeyPair,
  MissingHeaderError,
  pemToPrivateKey,
  pemToPublicKey,
  privateKeyToPem,
  publicKeyToPem,
  signRequest,
  verifyRequest,
} from './mod.ts';

const keys = await generateKeyPair({ bits: 1024 });

Deno.test('sign and verify', async () => {
  const request = new Request('https://soapbox.pub/inbox', {
    method: 'POST',
    body: '{"hello":"world"}',
  });

  await signRequest(
    request,
    keys.privateKey,
    'https://soapbox.pub/users/alex#main-key',
  );

  assert(request.headers.has('Signature'));
  assert(request.headers.has('Digest'));

  const verified = await verifyRequest(request, () => keys.publicKey);
  assert(verified);

  // Ensure the body hasn't been read.
  request.text();
});

Deno.test('verify', () => {
  const request = new Request('https://soapbox.pub/inbox', {
    method: 'POST',
    body: '{"hello":"world"}',
  });

  assertRejects(async () => {
    await verifyRequest(request, () => keys.publicKey);
  }, MissingHeaderError);
});

Deno.test('verify with bad signature', async () => {
  const now = Math.floor(Date.now() / 1000);

  const request = new Request('https://soapbox.pub/inbox', {
    method: 'POST',
    body: '{"hello":"world"}',
    headers: {
      host: 'soapbox.pub',
      digest: 'SHA-256=0bf474896363505e5ea5e5d6ace8ebfb13a760a409b1fb467d428fc716f9f284',
      signature:
        `created=${now},keyId="https://soapbox.pub/users/alex#main-key",algorithm="hs2019",headers="(request-target) (created) host digest",signature="nNQUnnddw8vA69kL6yZSZn60cQe4IfDKNeQ5C7tvbltevbkRMU3edXjnj0+Y7GDbyvOnNH63Skv2XvjmZLSCshP4DfYEyp+2LWQo99IyC4Jis/wlRPTIh1sXHgqUGJc/CqBc3ji3yF30yBixhmGcyHC1rx3bfqITmsXJymqpd/V2AC9fvD802oe3YVHfRXr1WUEarv55mZ/eLqk01YjD+MJeafHXOeFUwz54ji0I480TudCdMhJr3Pjm3iWS3lhWKm56rvRWeiel0aVWbP0d/VW5cGt1w99UtyO5utfrgeBxzWg5ZpXqwR5L6jh1xTo5pQJ06X6prdKhDYPKvzqqsA=="`,
    },
  });

  const verified = await verifyRequest(request, () => keys.publicKey);
  assert(!verified);
});

Deno.test('PEM public key conversion', async () => {
  const publicPem = await Deno.readTextFile('./fixtures/public.pem');
  const publicKey = await pemToPublicKey(publicPem);
  const publicPem2 = await publicKeyToPem(publicKey);
  assert(publicPem.startsWith(publicPem2.slice(0, 80)));
});

Deno.test('PEM private key conversion', async () => {
  const privatePem = await Deno.readTextFile('./fixtures/private.pem');
  const privateKey = await pemToPrivateKey(privatePem);
  const privatePem2 = await privateKeyToPem(privateKey);
  assert(privatePem.startsWith(privatePem2.slice(0, 80)));
});
